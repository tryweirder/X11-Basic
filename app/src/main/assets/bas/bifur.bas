' Bifurkationsdiagramm (c) Markus Hoffmann 2006
' Nach einer Idee von Robert May

bx%=0
by%=0
bw%=320
bh%=240

gelb=COLOR_RGB(1,1,0)
grau=COLOR_RGB(0.5,0.5,0.5)
weiss=COLOR_RGB(1,1,1)
rot=COLOR_RGB(1,0,0)

COLOR COLOR_RGB(0,0,0)
SIZEW 1,bw%,bh%
SHOWPAGE
PAUSE 0.1
GET_GEOMETRY 1,bx%,by%,bw%,bh%
PBOX bx%,by%,bw%,bh%
DIM dat(bw%/2+1)
COLOR grau
FOR i%=0 TO 4
  LINE i%*bw%/4,0,i%*bw%/4,bh%
NEXT i%
TEXT bw%/2,20,"bifur with X11-Basic (c) Markus Hoffmann"
COLOR weiss
' goto mmm
FOR y=0 TO bw%-1
  @doit
NEXT y
mmm:
weiss=rot
DO
  MOTIONEVENT x%,y%,k%
  y=x%+0.25*GASDEV()
  @doit
LOOP
QUIT
PROCEDURE doit
  LOCAL ox,r,x,c%
  r=y*4/bw%
  x=0.5
  c%=0
  FOR i%=0 TO bw%/2
    x=r*x*(1-x)
    dat(c%)=x
    INC c%
  NEXT i%
  COLOR schwarz
  PBOX 0,bh%/2,bw%/2,bh%
  COLOR gelb
  SCOPE dat(),0,bh%/2,bh%/2,1
  TEXT 50,bh%/2+50,STR$(r)
  COLOR weiss
  FOR i%=0 TO bh%
    ox=x
    x=r*x*(1-x)
    PLOT y,x*bh%
  NEXT i%
  SHOWPAGE
RETURN
