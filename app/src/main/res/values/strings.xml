<?xml version="1.0" encoding="utf-8"?>
<!-- This file is part of X11BASIC, the basic interpreter for Unix/X
 * ============================================================
 * X11BASIC is free software and comes with NO WARRANTY - read the file
 * COPYING for details
 -->
<resources>

    <string name="app_name">X11-Basic</string>
    <string name="app_versionname">X11-Basic V.1.27</string>
    <string name="impressum">
<![CDATA[ <h5>Contact</h5>
    X11-Basic for Android was brought to you by:<br/>
    Markus Hoffmann<br/>
    E-Mail: kollo@users.sourceforge.net<br/>
    Copyright &#169; 2009-2021 Markus Hoffmann
    <p/>
    X11-Basic for Android  comes with ABSOLUTELY NO WARRANTY;<br/>
    This is free software, and you are welcome to redistribute it
    under the conditions of the GNU GENERAL PUBLIC LICENSE, Version 2.
    ]]>
    </string>
    <string name="copyr">&#169; 1997&#8211;2021 Markus Hoffmann</string>
    
    <string name="readme">
<![CDATA[This is the Android port of X11-Basic, the BASIC programming language for UNIX/Linux.<p>
    X11-Basic is a dialect of BASIC with graphics capability and special features to make use of 
    the capapilities of the hardware platform (sensors) and operating system.  
    The syntax is most similar to the GFA-Basic in the ancient ATARI-ST 
    implementation. <br>
    <p> You can use the X11-Basic app as a formula pocket calculator or to run more 
    sophisticated programs with graphics. 
    There is no editor included in this package. So you need to write your basic 
    sourcecode with an external (third party) editor. The files should have a 
    filename with .bas extension and they should be placed into the /storage/bas 
    folder.
<p> Then they can be loaded with the X11-Basic app (Menu-->load) and run (Menu-->run).
    Basic programs can also be compiled into a binary bytecode form, which can be 
    run 20 times faster.<p> 
    The compiled code is very fast (no faster BASIC interpreter is currently known for Android). <p> 
    See the project home page for more details: 
    <a href="http://x11-basic.sourceforge.net/">http://x11-basic.sourceforge.net/</a>
    (A full manual can be downloaded from the home page.)
    <p>Please also use the discussion pages and X11-Basic forum at sourceforge for feedback. <p>
    <p>The most recent version of X11-Basic for Android can be found on the project pages
       on sourceforge as .apk file.
    <p>There is also a .zip file with lots of example programs. 
    (Download it form this page: 
    <a href="http://sourceforge.net/projects/x11-basic/files/X11-Basic-programs/">http://sourceforge.net/projects/x11-basic/files/X11-Basic-programs/</a> )
    <p>
X11-BASIC is free software and comes with NO WARRANTY - read the file
<a href="http://x11-basic.sourceforge.net/COPYING">http://x11-basic.sourceforge.net/COPYING</a> 
for details. <br>
(Basically that means, free, open source, use and modify as you like, don\'t
incorporate it into non-free software, no warranty of any sort, don\'t blame me
if it doesn\'t work.)
    <p>
]]></string>
    <string name="news">
<![CDATA[<h4>News</h4>  
    There are probably some bugs with this Android version of X11-Basic which do 
    not appear in the 
    PC Versions and which havn\'t been solved so far. Be patient!
    Not all graphics has been ported yet, some of the input methods (mouse) are 
    still stub. 
    <p>
    The libgmp functions (big number calculations) had to be removed in the version
    of X11-Basic which is distributed in the F-Droid App Store. This is not because 
    libgmp is malicious or anything like that, but simply, because so far, it could
    not be compiled automatically from sources. However, the versions of X11-Basic 
    distributed from the sourceforge pages and in Google Play have working big 
    number calculus (from a prebuild libgmp). 
    <p>
    PRINT and INPUT on the stdin and stdout have been directed to a simulated 
    VT100 terminal (including code from TTconsole -- The Tomtom console).
    <p>
	Special Android Hardware features can be accessed with GPS, SPEAK and SENSOR(). 
    <p>You can help me to improve this App: Please be more verbose when posting comment to bug or crash 
    reports. Use the projects discussion pages and the forum on sourceforge. The Project is open source, so also 
    code improvements are very welcome.
    <H4>Acknowledgements</h4>
    Thanks to all people, who helped me to realize this package.
<p>
Many  thanks  to  the developers of GFA-Basic. This basic made me
start programming in the 1980s. Many ideas and most of  the  command 
syntax has been taken from the ATARI ST implementation.
<p>
Thanks to sourceforge.net for hosting this project on the web.
<p>
Special thanks to following people, who helped me recently:<br>
in 2013:<br>
* Matthias Vogl (va_copy patch for 64bit version)<br>
* Eckhard Kruse (for permission to include ballerburg.bas in the samples)<br>
* Marcos Cruz (beta testing and bug fixing)<br>
* James V. Fields (beta testing and correcting the manual)<br>
in 2014:<br>
* John Clemens, Sławomir Donocik, Izidor Cicnapuz, Christian Amler,<br> 
  Marcos Cruz,  Charles Rayer, Bill Hoyt, and John Sheales (beta testing and bug fixing)
  <br>
in 2015:<br>
* Guillaume Tello, Wanderer, and John Sheales  (beta testing and bug fixing)<br>
in 2016:<br>
* John Sheales  (beta testing and bug fixing)<br>
* bruno xxx (helping with the compilation tutorial for Android)<br>
in 2017:<br>
* John Sheales  (beta testing and bug fixing)<br>
* David Hall  (bug fixing)<br>
* Emil Schweikerdt  (bug fixing)<br>
in 2018:<br>
* Alan (beta testing, bugfix)<br>
* John Sheales  (beta testing and bug fixing)<br>
in 2019:<br>
* Yet Another Troll (beta testing and bug fixing)<br>
in 2020:<br>
* amaendle (bug fixing)<br>
* John Sheales  (beta testing and bug fixing)<br>


<p>
X11-Basic is build on top of many free softwares, and could not exist without 
them. 
<p>
The Android port of X11-Basic comes with a port of the gmp library, 
the GNU multiple precision arithmetic library, Version 5.1.3.
   Copyright 1991, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
2001, 2002, 2003, 2004, 2005, 2006, 2007 Free Software Foundation, Inc.

For details, see: https://gmplib.org/
<p>
LAPACK (Linear Algebra Package) is a standard software library for 
numerical linear algebra. 

For details, see: http://www.netlib.org/lapack/
<p>
X11-Basic uses the lodepng code for the PNG bitmap graphics support.
Copyright &#169; 2005-2013 Lode Vandevenne
<p>

<h4>Release notes for Version 1.27 (June 2019 -- Jan 2021)</h4>
- removed VERSION and VERSION_DATE from library<br>
- new function REGEXP() and MATCH()<br>
- little fix for HELP, REM and DATA<br>
- New feature: ANS variable (store result of simple calculation)<br>
- New function VARLEN() <br>
- New function CRC16() <br>
- New mqtt commands BROKER, SUBSCRIBE, PUBLISH<br>

    ]]>
    </string>
    <string name="menu_about">About</string>
    <string name="menu_quit">Quit</string>
    <string name="menu_run">Run</string>
    <string name="menu_load">Load &#8230;</string>
    <string name="menu_stop">Stop/Cont</string>
    <string name="menu_new">New</string>
    <string name="menu_compile">Compile</string>
    <string name="menu_editor">Editor</string>
    <string name="menu_help">Help&#8230;</string>
    <string name="menu_settings">Info/Settings &#8230;</string>
    <string name="menu_fontsize">Select font size</string>
    <string name="menu_focus">Select screen focus</string>
    <string name="menu_splashscreen">Splash screen</string>
    <string name="menu_splashscreen2">Show version info on X11-Basic startup.</string>
    <string name="menu_keyboard">keyboard</string>
    <string name="menu_paste">Paste from Clipboard</string>

    <string name="menu_dispkey">Show keyboard</string>
    <string name="menu_dispkey2">Show keyboard at program start</string>
    <string name="menu_dispheader">Show title</string>
    <string name="menu_dispheader2">Show title bar</string>
    <string name="menu_dispstatus">Show status bar</string>
    <string name="menu_dispstatus2">Show status bar</string>
    
    <string name="word_settings">Settings</string> 
    <string name="word_search">Search:</string>
    <string name="word_searchcomment">Enter Keyword or Command</string>
    <string name="word_ok">OK</string>
    <string name="word_keyword">BASIC keyword</string>
    <string name="word_version">Version</string>
    <string name="word_copyright">Copyright</string>
    
    <string name="about_market_app">This app on the market</string>
    <string name="about_homepage">Project Homepage</string>
    <string name="about_guestbook">Guestbook</string>
    <string name="about_wiki">X11-Basic Forum</string>
    <string name="about_manual">The Manual</string>
    <string name="about_market_publisher">More apps from the author of this one</string>
    <string name="about_example_programs">X11-Basic example programs (source code)</string>
    <string name="preferences_credits_ttconsole_title">Credits: The TomTom Console</string>
    <string name="edit_text">Edit text</string>
    <string name="select_text">Select text</string>
    <string name="copy_all">Copy all</string>
    <string name="paste">Paste</string>
    <string name="editor_notfound">
<![CDATA[<h4>Editor not found!</h4> 
       There is no text editor installed, which can be used. 
       You can install your favorite one. 
       If you do not know which is best, take the app called "920 Text Editor". This works.
    ]]>
    </string>
    <string name="editor_notb">
<![CDATA[<h2>Error!</h2> 
       You cannot edit compiled (binary) code! Please load the corresponding .bas source file, 
       edit and recompile it. 
    ]]></string>
    <string name="sdcard_notb">
<![CDATA[<h2>WARNING!</h2> 
       Your external storage directory (sdcard) seems to be not usable.
       X11-Basic will try to use the internal storage instead. But it then will
       not be possible to use external sourcecode, and also you might not be
       able to edit X11-Basic programs or write your own ones. Further, when
       X11-Basic is deinstalled from the device, all changes to the files in
       internal storage including newly created ones will get removed along with
       the app. Please check if the files in {0} can be accessed from other
       apps, and if so, always make backups of your work.
    ]]>
    </string>
    <string name="word_cancel">Cancel</string>
    <string name="word_proceed">Proceed</string>
    

    <!-- Crash report dialog -->
    <string name="crash_subject">Crash: X11-Basic v{0} on {1} / {2} / {3}</string>
    <string name="crash_preamble">Please describe what you were doing just before the crash if possible, and then press Send:</string>
    <string name="crashtitle">Well, this is embarrassing&#8230;</string>
    <string name="crashed">Sorry, something caused X11-Basic to seriously crash. It had to close. If you think
        it was not your fault (e.g. using POKE or something like this) you might like to send a 
        crash report, which may help the author to fix the problem?</string>
    <string name="report">Report</string>
    <string name="close">Close</string>
    <string name="author_email">dr.markus.hoffmann@gmx.de</string>
    <string name="email_subject">X11Basic v{0} on {1} / {2} / {3}</string>
    <string name="unknown_version">UNKNOWN</string>
    
    <!-- Messages id X11BasicActivity -->
    
    <string name="word_interrupt">Interrupt.</string>
    <string name="word_done">done.</string>
    <string name="message_shortcut_created">Shortcut \'%s\' on Desktop created.</string>
    <string name="message_nothing_to_do">Empty program. Nothing to do.</string>
    <string name="message_nothing_to_do2">No program specified. Nothing to do.</string>
    <string name="message_compilation_success">Compilation successful. Saved to %s.</string>
    <string name="error_backgroundtask_dead">ERROR: Background task seems to be dead.</string>
    <string name="error_compilation">ERROR: compilation failed.</string>
    <string name="error_editor">ERROR: the editor could not be launched. Message: %s</string>
    <string name="error_intend">ERROR: the intend could not be launched. Message: %s</string>
    <string name="message_really_delete">Really delete ?</string>
    <string name="message_delete">You are about to delete the file %s. Do you really want to proceed?</string>
    <string name="message_select_file_shortcut">Select a File for Shortcut:</string>
    <string name="message_select_file_load">Select File to load:</string>
    <string name="message_reloaded">%s has been reloaded.</string>
    
</resources>
